package com.test.gennadiystrizhak.testnodejs;

import android.app.Activity;
import android.os.Bundle;
import android.provider.Settings;

import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import io.socket.IOAcknowledge;
import io.socket.IOCallback;
import io.socket.SocketIO;
import io.socket.SocketIOException;

import static android.view.View.OnClickListener;


public class MainActivity extends Activity {

    private final String URL = "http://asdgroup.pro:4488";

    private String[] actions = { "Login", "SendMessage", "I'm Typing", "I change Back"};

    private SocketIO socket;
    private EditText fromUser;
    private EditText toUser;
    private EditText output;
    private EditText deviceID;
    private EditText message;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        output = (EditText) findViewById(R.id.resultET);
        fromUser = (EditText) findViewById(R.id.fromET);
        toUser = (EditText) findViewById(R.id.toET);
        deviceID = (EditText) findViewById(R.id.device_id);
        message = (EditText) findViewById(R.id.messageET);

        try {
            socket = new SocketIO();
            socket.connect("http://asdgroup.pro:4488", new IOCallback() {
                @Override
                public void onDisconnect() {
                    Log.d("Disconnected", ":(");
                }

                @Override
                public void onConnect() {
                    Log.d("Connected", ":)");
                }

                @Override
                public void onMessage(String s, IOAcknowledge ioAcknowledge) {
                    Log.d(/**/"RECEIVE", s);
                }

                @Override
                public void onMessage(JSONObject jsonObject, IOAcknowledge ioAcknowledge) {
                    Log.d("RECEIVE", "jsonObject");

                    try {
                        String user_id =  jsonObject.getString("from_user_id");
                        String text = jsonObject.getString("text");
                        output.setText( user_id + ":" + text);
                    }
                    catch (JSONException e) {
                        e.printStackTrace();
                    }
                        //Log.d("RECEIVE :", user_id);
                }

                @Override
                public void on(String s, IOAcknowledge ioAcknowledge, Object... objects) {
                    Log.d("SocketIO", s);
                }

                @Override
                public void onError(SocketIOException e) {
                    Log.e( "onError:", e.getMessage() );
                }
            });
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        Button loginBtn = (Button) findViewById(R.id.loginBtn);
        loginBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                JSONObject json = new JSONObject();
                try {
                    json.put("user_id", fromUser.getText());
                    json.put("device_id", deviceID.getText());
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                socket.emit("connected",json);
            }
        });
        Button sendBtn = (Button) findViewById(R.id.sendBtn);
        sendBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                JSONObject json = new JSONObject();
                try {
                    json.put( "to_user_id", toUser.getText() );
                    json.put( "from_user_id",fromUser.getText() );
                    json.put( "text", message.getText() );
                }
                catch (JSONException e) {
                    e.printStackTrace();
                }
                socket.emit("send_message", json);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
